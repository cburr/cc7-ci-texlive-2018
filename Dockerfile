FROM gitlab-registry.cern.ch/olupton/cc7-ci-dockerfile
ENV TL /install-tl-latest
RUN mkdir -p $TL
ADD full.profile $TL/
RUN cd $TL/ \
  && wget -nv -O $TL.tar.gz http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz \
  && tar -xzf $TL.tar.gz --strip-components=1 \
  && ./install-tl --persistent-downloads --profile full.profile \
  && cd / \
  && rm -rf $TL
ENV PATH $PATH:/usr/local/texlive/2018/bin/x86_64-linux
RUN tlmgr install was tikzinclude xstring genmisc multirow isodate substr relsize textpos biblatex logreq \
  biblatex-phys minted fvextra upquote ifplatform framed biber varwidth varsfromjobname currfile datetime2 tracklang \
  datetime2-english latexmk hyperxmp cleveref glossaries mfirstuc xfor datatool wrapfig pgfgantt overpic eepic ifsym \
  placeins feynmf nopageno ifmtarg metapost pdfcrop pdfjam latexpand appendixnumberbeamer
RUN [ -f $(kpsewhich -var-value TEXMFSYSVAR)/fonts/conf/texlive-fontconfig.conf ] && \
  cp $(kpsewhich -var-value TEXMFSYSVAR)/fonts/conf/texlive-fontconfig.conf /etc/fonts/conf.d/09-texlive.conf || :
RUN fc-cache -fsv
